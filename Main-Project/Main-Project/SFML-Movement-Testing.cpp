#include <iostream>

#include <SFML\Graphics.hpp>

using namespace std;

int main()
{
	sf::RenderWindow mainWindow(sf::VideoMode(1280, 720), "SFML Movement Test");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Cyan);
	shape.setOrigin(-640, -360);
	mainWindow.setFramerateLimit(120);

	while (mainWindow.isOpen())
	{
		sf::Event event;
		//Runs Then there are events pending on the main window
		while (mainWindow.pollEvent(event))
		{
			//Switch that handles the different event types that can happen
			switch (event.type)
			{
				//Case that takes care of the instance when the mainWindow closes
			case sf::Event::Closed:
			{
				mainWindow.close();	//Destroys the window and all it's resources
				break;
			}

			//Handles the events for the different key presses that happens
			case sf::Event::KeyPressed:
			{
				if (event.key.code == sf::Keyboard::W)
				{
					cout << "W key pressed" << endl;
					shape.move(0, -120);
				}

				if (event.key.code == sf::Keyboard::A)
				{
					cout << "A key pressed" << endl;
					shape.move(-120, 0);
				}

				if (event.key.code == sf::Keyboard::S)
				{
					cout << "S key pressed" << endl;
					shape.move(0, 120);
				}

				if (event.key.code == sf::Keyboard::D)
				{
					cout << "D key pressed" << endl;
					shape.move(120, 0);
				}

				break;
			}//End of keypressed case



				//Default case aka event that will not be handled here
			default:
			{
				break;
			}
			}//End of switch statement for event handling
		}

		mainWindow.clear();
		mainWindow.draw(shape);
		mainWindow.display();
	}

	return 0;
}