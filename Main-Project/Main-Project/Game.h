#pragma once

#include "Creature.h"
#include "Monster.h"
#include "Player.h"
#include "Terrain.h"
#include "PlayerStatusBar.h"
#include "Matrix2D.h"

#include <SFML\Graphics.hpp>

using namespace std;

class Game : public sf::Drawable
{
public:
	Game();
	~Game();

	int getRectangleSideLength();

	sf::Vector2i getMapSize();

	string getMapName();

	int getOffSet();

	//Main function for building the map, this function will call the other functions for map creation
	bool buildMap();

	//Loads the mapTextures from a file
	bool buildMapTextureGrid(string mapTexturePath);
	
	//Builds the tileGridOverlay so you can se the border between tiles
	bool buildTileGridOverlay();
	
	//Loads the position of the entyties from a file and builds the objects
	bool buildEntityGrid(string mapEntityPath);

	//Help function to load enityData, instead of duplicateing code in every case
	void assignEntityData(ifstream &file, int &damageBuffer, int &moneyDropBuffer, int &xpDropBuffer, int &hpBuffer, int & levelBuffer);

	void update(sf::Event event);	//Handles the gamelogic like asking for collision with monsters and make sure the player battles the monster if collision ocurs

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const;	//Draws the entire map from the 3 grids created and the status bar

	void gameOverScreenCreation();	//Will be ran if the exception gameover is thrown

private:
	int rectangleSideLenght;
	int mapHeight;
	int mapWidth;

	string textureMapPath;
	string entityMapPath;
	string mapName;

	int offset = 120;

	bool creatureHit;
	bool battleDone = false;
	bool gameOver = false;

	Player* playerPtr;

	Monster* monsterArray[12];
	int numberOfMonstersInMap;

	sf::RectangleShape* gameOverScreen;
	sf::Text gameOvertext;
	sf::Font gameoverFont;	

	//Statusbar that will display playerdata
	PlayerStatusBar* statusBar;

	//The three different grids that will be used to create the map
	Matrix2D<Terrain*>* terrainGrid;
	Matrix2D<sf::RectangleShape>* tileGridOverlay;
	Matrix2D<Creature*>* entityGrid;
};