#pragma once

#include <SFML\Graphics.hpp>

using namespace std;

class Terrain : public sf::Drawable
{
public:
	Terrain(bool isPassable = true);

	//Terrain constructor for textures
	Terrain(string texture, const int i, const int j, const int rectangeLength);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const;

	~Terrain();

private:
	bool isPassable;

	sf::Texture terrainTexture;
	sf::Sprite terrain;
};