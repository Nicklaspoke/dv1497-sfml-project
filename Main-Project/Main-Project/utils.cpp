#include <random>

#include "utils.h"

using namespace std;

int randomize(int min_value, int max_value)
{
	random_device rand_dev;
	mt19937 mt(rand_dev());
	uniform_int_distribution<int> dist(min_value, max_value);

	return dist(mt);
}