#include <iostream>

#include <SFML\Graphics.hpp>

using namespace std;

const float RECTANGLEWITH = 120;
const float RECTANGLEHEIGHT = 120;

const int WINDOWHEIGHT = RECTANGLEHEIGHT * 5;
const int WINDOWWITH = RECTANGLEWITH * 5;

int main()
{
	sf::RectangleShape mainMap[5][5];
	
	sf::RenderWindow mainWindow(sf::VideoMode(WINDOWWITH, WINDOWHEIGHT), "Maprender Test");
	mainWindow.setFramerateLimit(120);
	/*ifstream fileRead;

	fileRead.open("Map-Read-Test.txt");
	string readbuffer;
	string colorbuffer;
	
	//Creating a cyan colored rectangle
	sf::RectangleShape rectangle1;
	rectangle1.setSize(sf::Vector2f(40, 40));
	rectangle1.setFillColor(sf::Color::Cyan);
	rectangle1.setPosition(0, 0);

	//creating a yellow colored rectangle
	sf::RectangleShape rectangle2;
	rectangle2.setSize(sf::Vector2f(40, 40));
	rectangle2.setFillColor(sf::Color::Yellow);
	rectangle2.setPosition(40, 0);*/

	sf::CircleShape testMove(RECTANGLEHEIGHT/3);
	testMove.setFillColor(sf::Color::Cyan);
	//testMove.move(260, 260);
	testMove.setOrigin(-260, -260);

	//Creating the tiles for the map
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			sf::RectangleShape iRecShape;
			iRecShape.setSize(sf::Vector2f(RECTANGLEHEIGHT, RECTANGLEWITH));
			iRecShape.setFillColor(sf::Color::Green);
			iRecShape.setOutlineThickness(1);
			iRecShape.setOutlineColor(sf::Color::Black);
			iRecShape.setPosition(RECTANGLEHEIGHT * i, RECTANGLEWITH * j);
			mainMap[i][j] = iRecShape;
		}
	}
	

	//mainMap[0][0] = rectangle1;
	//mainMap[0][1] = rectangle2;

	while (mainWindow.isOpen())
	{
		sf::Event event;

		while (mainWindow.pollEvent(event))
		{
			//Switch to handle all the events
			switch (event.type)
			{
				//Case for destroying the mainWindow
			case sf::Event::Closed:
			{
				mainWindow.close();
				break;
			}

			case sf::Event::KeyPressed:
			{
				if (event.key.code == sf::Keyboard::W)
				{
					cout << "W key pressed" << endl;
					testMove.move(0, -RECTANGLEHEIGHT);
				}

				if (event.key.code == sf::Keyboard::A)
				{
					cout << "A key pressed" << endl;
					testMove.move(-RECTANGLEHEIGHT, 0);
				}

				if (event.key.code == sf::Keyboard::S)
				{
					cout << "S key pressed" << endl;
					testMove.move(0, RECTANGLEHEIGHT);
				}

				if (event.key.code == sf::Keyboard::D)
				{
					cout << "D key pressed" << endl;
					testMove.move(RECTANGLEHEIGHT, 0);
				}

				break;
			}//End of keypressed Case

			default:
			{
				break;
			}
			}//End of switch statement

		}//End of loop for pollEvent

		//mainWindow.clear();
		mainWindow.clear();

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				mainWindow.draw(mainMap[i][j]);
			}
		}
		mainWindow.draw(testMove);
		mainWindow.display();
	}


}