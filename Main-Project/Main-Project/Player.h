#pragma once
#include <string>

#include<SFML\Graphics.hpp>

#include "Creature.h"
#include "Monster.h"

using namespace std;

class Player : public Creature
{
public:
	Player(sf::Vector3i position, int id, string name = "N/A", int hp = 100, int level = 1, int money = 1000, int xp = 0, int damage = 10);
	~Player();

	int getMoney();
	int getXP();
	int getPlayerDamage();
	void changeXP(int xp_change);
	void changeMoney(int money_change);

	//Levels up the Player if they have enough xp
	void levelUp();

	//Battles with the monster the Player encountered in the hit function in Creature class
	bool battle(Monster& monsterToBattle);

private:
	int money;
	int xp;
	int playerDamage;

	
};

