#include <iostream>
#include <fstream>
#include <sstream>

#include <SFML\Graphics.hpp>
#include <SFML\Graphics\Color.hpp>

#include "Game.h"
#include "Player.h"
#include "Monster.h"
#include "Creature.h"
#include "PlayerStatusBar.h"
#include "utils.h"

using namespace std;

Game::Game()
{
	int mapChoice = -1;
	//Letting the player choose a map to load, assuming to user don't input incorrect data
	cout << "Which Map to load?: " << endl;
	cout << "1. Map 1:" << endl << "\tMapname: Outside" << endl << "\tSize: 5x5" << endl << "\tNumber of entities: 5" << endl;
	cout << "2. Map 2:" << endl << "\tMapname: Inside Tower" << endl << "\tSize: 6x6" << endl << "\tNumber of entities: 7" << endl;
	cout << "3. Map 3:" << endl << "\tMapname: Dungeon 1" << endl << "\tSize: 7*7" << endl << "\tNumber of entities: 12" << endl;
	cout << "Map to load: ";
	cin >> mapChoice;
	cin.ignore();

	this->numberOfMonstersInMap = 0;

	//Constructs a string and sends it to mapbuilder acording to which map was selected
	switch (mapChoice)
	{
	//Load Map 1
	case 1:
	{
		this->textureMapPath = "../Resources/Maps/Map_1.texmap";
		this->entityMapPath = "../Resources/Maps/Map_1.entmap";

		buildMap();
		break;
	}

	//Load Map 2
	case 2:
	{
		this->textureMapPath = "../Resources/Maps/Map_2.texmap";
		this->entityMapPath = "../Resources/Maps/Map_2.entmap";

		buildMap();
		break;
	}

	//Load Map 3
	case 3:
	{
		this->textureMapPath = "../Resources/Maps/Map_3.texmap";
		this->entityMapPath = "../Resources/Maps/Map_3.entmap";

		buildMap();
		break;
	}

	}//End of mapselector switch
	//After entities and map has been loaded in, a status bar in the bottom of the screen is created
	this->statusBar = new PlayerStatusBar(sf::Vector3i(this->mapHeight, this->mapWidth, this->rectangleSideLenght), this->offset, *this->playerPtr);
}


Game::~Game()	//Destructor, takes care of destucting all the grids and and statusbar, also the gameover screen if that was triggered
{
	for (int i = 0; i < mapHeight; i++)
	{
		for (int j = 0; j < mapWidth; j++)
		{
			delete terrainGrid->accessAt(i, j);
			delete entityGrid->accessAt(i, j);
		}
	}

	delete terrainGrid;
	delete tileGridOverlay;
	delete entityGrid;

	delete statusBar;
	
	if (this->gameOver)
	{
		delete gameOverScreen;
	}
}

int Game::getRectangleSideLength()
{
	return this->rectangleSideLenght;
}

sf::Vector2i Game::getMapSize()
{
	return sf::Vector2i(this->mapWidth, this->mapHeight);
}

string Game::getMapName()
{
	return this->mapName;
}

int Game::getOffSet()
{
	return this->offset;
}

bool Game::buildMap()
{
	//Main bool that is returned true if all parts of the mapcreation is successful
	bool mapCreationSuccess = false;

	//Bools to check if the different parts of the map creation is successful
	bool mapTextureGridSuccess = false;
	bool tileGridOverlaySuccess = false;
	bool entityGridSuccess = false;

	if (buildMapTextureGrid(this->textureMapPath))
	{
		cout << "Creation Of Texture Grid Successful" << endl;
		mapTextureGridSuccess = true;
	}

	if (buildTileGridOverlay())
	{
		cout << "Creation of Tile Grid Overlay Successful" << endl;
		tileGridOverlaySuccess = true;
	}

	if (buildEntityGrid(this->entityMapPath))
	{
		cout << "Creation of Entity Grid Successful" << endl;
		entityGridSuccess = true;
	}

	if (mapTextureGridSuccess && tileGridOverlaySuccess && entityGridSuccess)
	{
		cout << "All Mapgrids Created Successfully" << endl;
		mapCreationSuccess = true;
	}

	return mapCreationSuccess;
}

//Loads map from a file with the filename and path mapPath, 
//returns true if maploading is succesful and false if there was a problem with loading the map
bool Game::buildMapTextureGrid(string mapPath)
{
	bool mapTextureSuccess = false;
	ifstream mapReader;
	string buffer;

	sf::Vector3i position;

	mapReader.open(mapPath);

	if (!mapReader)
	{
		cout << "Could not open maptexture file" << endl;
	}
	else
	{
		getline(mapReader, this->mapName);

		mapReader >> this->mapHeight;
		mapReader.ignore();

		mapReader >> this->mapWidth;
		mapReader.ignore();

		mapReader >> this->rectangleSideLenght;
		mapReader.ignore();

		position.z = this->rectangleSideLenght;

		this->terrainGrid = new Matrix2D<Terrain*>(this->mapHeight, this->mapWidth);

		for (int i = 0; i < mapHeight; i++)
		{
			for (int j = 0; j < mapWidth; j++)
			{
				getline(mapReader, buffer);

				position.x = i;
				position.y = j;

				terrainGrid->assignElementAt(i, j, new Terrain(buffer, i,j, this->rectangleSideLenght));
			}
		}

		mapTextureSuccess = true;
	}

	mapReader.close();
	return mapTextureSuccess;
}

//Builds the tileGridOverlay so you can se the border between tiles
bool Game::buildTileGridOverlay()
{
	bool tileGridOverlaySuccess = false;

	//Creating the grid overlayfor the map
	this->tileGridOverlay = new Matrix2D<sf::RectangleShape>(this->mapHeight, this->mapWidth);
	for (int i = 0; i < this->mapHeight; i++)
	{
		for (int j = 0; j < this->mapWidth; j++)
		{
			sf::RectangleShape gridRec;
			gridRec.setSize(sf::Vector2f(this->rectangleSideLenght, this->rectangleSideLenght));
			gridRec.setOutlineThickness(3);
			gridRec.setOutlineColor(sf::Color::Black);
			gridRec.setFillColor(sf::Color::Transparent);
			gridRec.setPosition(this->rectangleSideLenght * i, this->rectangleSideLenght * j);

			tileGridOverlay->assignElementAt(i, j, gridRec);

		}

		tileGridOverlaySuccess = true;
	}
	
	return tileGridOverlaySuccess;
}

//Builds the grid for the entites
bool Game::buildEntityGrid(string mapEntityPath)
{
	bool entityGridSuccess = false;
	ifstream entityMapReader;
	int idbuffer;
	string buffer;

	sf::Vector3i position;
	position.z = this->rectangleSideLenght;

	sf::Vector2i playerPosition;

	//Buffers for the different values that will be read into a monster entity
	int damageBuffer;
	int moneyDropBuffer;
	int xpDropBuffer;
	int hpBuffer;
	int levelBuffer;

	entityMapReader.open(mapEntityPath);

	//Creating the tilegrid for entities
	this->entityGrid = new Matrix2D<Creature*>(this->mapHeight, this->mapWidth);

	if (!entityMapReader)
	{
		cout << "Could not open enitymap file" << endl;
	}
	
	for (int i = 0; i < this->mapHeight; i++)
	{
		for (int j = 0; j < this->mapWidth; j++)
		{
			entityMapReader >> idbuffer;
			entityMapReader.ignore();

			position.x = i;
			position.y = j;

			//The file will contain a ID for which entity that will 
			switch (idbuffer)
			{
			//Case 0: Player creation
			case 0:
			{
				ifstream loadEntityData;

				loadEntityData.open("../Resources/Entity Info/Player Data.entity");
				
				int BasehpBuffer;	//Hp will be calculated through the following, HP = BaseHp * 1,20 per level
				int levelbuffer;
				int moneyBuffer;
				int xpBuffer;
				int basedamageBuffer;	//Damage will be calculated through the following, baseDamage * level

				//Loading all the data before sending it to the constructor
				getline(loadEntityData, buffer);

				loadEntityData >> BasehpBuffer;
				loadEntityData.ignore();

				loadEntityData >> levelbuffer;
				loadEntityData.ignore();

				loadEntityData >> moneyBuffer;
				loadEntityData.ignore();

				loadEntityData >> xpBuffer;
				loadEntityData.ignore();

				loadEntityData >> basedamageBuffer;
				loadEntityData.ignore();

				this->entityGrid->assignElementAt(i, j, new Player(position, idbuffer, buffer, BasehpBuffer, levelbuffer, moneyBuffer, xpBuffer));
				
				this->playerPtr = dynamic_cast<Player*>(this->entityGrid->accessAt(i, j));

				playerPosition.x = i;
				playerPosition.y = j;

				loadEntityData.close();
				break;
			}//end of case 0

			//Case 1: Slime creation
			case 1:
			{
				ifstream loadEntityData;

				loadEntityData.open("../Resources/Entity Info/Slime.entity");

				getline(loadEntityData, buffer);

				this->assignEntityData(loadEntityData, damageBuffer, moneyDropBuffer, xpDropBuffer, hpBuffer, levelBuffer);

				entityGrid->assignElementAt(i, j, new Monster(position, idbuffer, buffer, hpBuffer, levelBuffer, damageBuffer, moneyDropBuffer, xpDropBuffer));

				this->monsterArray[numberOfMonstersInMap] = dynamic_cast<Monster*>(this->entityGrid->accessAt(i, j));
				this->numberOfMonstersInMap++;

				loadEntityData.close();
				break;
			}//end of case 1

			//Case 2: Zombie creation
			case 2:
			{
				ifstream loadEntityData;

				loadEntityData.open("../Resources/Entity Info/Zombie.entity");

				getline(loadEntityData, buffer);

				//Loads and assigns damage based on the range given
				this->assignEntityData(loadEntityData, damageBuffer, moneyDropBuffer, xpDropBuffer, hpBuffer, levelBuffer);

				entityGrid->assignElementAt(i, j, new Monster(position, idbuffer, buffer, hpBuffer, levelBuffer, damageBuffer, moneyDropBuffer, xpDropBuffer));

				this->monsterArray[numberOfMonstersInMap] = dynamic_cast<Monster*>(this->entityGrid->accessAt(i, j));
				this->numberOfMonstersInMap++;

				loadEntityData.close();
				break;
			}//end of case 2

			//Case 3: Knight creation
			case 3:
			{
				ifstream loadEntityData;

				loadEntityData.open("../Resources/Entity Info/Knight.entity");

				getline(loadEntityData, buffer);

				this->assignEntityData(loadEntityData, damageBuffer, moneyDropBuffer, xpDropBuffer, hpBuffer, levelBuffer);

				entityGrid->assignElementAt(i, j, new Monster(position, idbuffer, buffer, hpBuffer, levelBuffer, damageBuffer, moneyDropBuffer, xpDropBuffer));

				this->monsterArray[numberOfMonstersInMap] = dynamic_cast<Monster*>(this->entityGrid->accessAt(i, j));
				this->numberOfMonstersInMap++;

				loadEntityData.close();
				break;
			}//End of case 3

			//Case 4: Dragon creation
			case 4:
			{
				ifstream loadEntityData;

				loadEntityData.open("../Resources/Entity Info/Dragon.entity");

				getline(loadEntityData, buffer);

				this->assignEntityData(loadEntityData, damageBuffer, moneyDropBuffer, xpDropBuffer, hpBuffer, levelBuffer);

				entityGrid->assignElementAt(i, j, new Monster(position, idbuffer, buffer, hpBuffer, levelBuffer, damageBuffer, moneyDropBuffer, xpDropBuffer));

				this->monsterArray[numberOfMonstersInMap] = dynamic_cast<Monster*>(this->entityGrid->accessAt(i, j));
				this->numberOfMonstersInMap++;

				loadEntityData.close();
				break;
			}//End of case 4

			//Case 99: Nothing in this tile
			case 99:
			{
				this->entityGrid->assignElementAt(i, j, nullptr);
				break;
			}//End of case 99
			}//End of switch
		}

		entityGridSuccess = true;
	}
	entityMapReader.close();

	return entityGridSuccess;
}

//Loads entity data from the file it was given
void Game::assignEntityData(ifstream &file, int &damageBuffer, int &moneyDropBuffer, int &xpDropBuffer, int &hpBuffer, int & levelBuffer)
{
	int lowerRandomRange;
	int uppRandomRange;

	//Loads and assigns damage based on the range given
	file >> lowerRandomRange;
	file.ignore();

	file >> uppRandomRange;
	file.ignore();

	damageBuffer = randomize(lowerRandomRange, uppRandomRange);

	//Loads and assigns moneydrop based on the range given
	file >> lowerRandomRange;
	file.ignore();

	file >> uppRandomRange;
	file.ignore();

	moneyDropBuffer = randomize(lowerRandomRange, uppRandomRange);

	//Loads and assigns xpdrop based on the range given
	file >> lowerRandomRange;
	file.ignore();

	file >> uppRandomRange;
	file.ignore();

	xpDropBuffer = randomize(lowerRandomRange, uppRandomRange);

	//Loads and assigns hp based on the range given
	file >> lowerRandomRange;
	file.ignore();

	file >> uppRandomRange;
	file.ignore();

	hpBuffer = randomize(lowerRandomRange, uppRandomRange);

	//Loads and assigns level based on the range given
	file >> lowerRandomRange;
	file.ignore();

	file >> uppRandomRange;
	file.ignore();

	levelBuffer = randomize(lowerRandomRange, uppRandomRange);
}

//Takes the event and runs collision check on the tile next to the player based on direction the player wanted to move
void Game::update(sf::Event event)
{
	this->creatureHit = false;
	this->battleDone = false;
	int i = 0;
	
	if ((event.key.code == sf::Keyboard::W) && (this->playerPtr->getCreaturePositionX() -1 >= 0))
	{	
		while (i < numberOfMonstersInMap && !creatureHit)
		{
			if (monsterArray[i] != nullptr && playerPtr->hit(monsterArray[i], 1))
			{
				creatureHit = true;
			}
			else
			{
				i++;
			}
		}

		if (creatureHit)
		{
			try
			{
				battleDone = playerPtr->battle(*monsterArray[i]);
			}
			catch (char* message)
			{
				cout << "Game Over Triggered" << endl;
				this->gameOver = true;
				this->gameOverScreenCreation();
				this->statusBar->gameOverTrigger();
			}
				
		}

		if (battleDone)
		{
			int tempI, tempJ;
			tempI = this->monsterArray[i]->getCreaturePositionX();
			tempJ = this->monsterArray[i]->getCreaturePositionY();

			delete this->entityGrid->accessAt(tempI, tempJ);
			this->entityGrid->assignElementAt(tempI, tempJ, nullptr);
			this->monsterArray[i] = nullptr;

			swapElements(monsterArray[i], monsterArray[numberOfMonstersInMap-1]);
			numberOfMonstersInMap--;

			//Checks if the player has leveld up
			if (this->playerPtr->getXP() >= (playerPtr->getLevel() * 50))
			{
				playerPtr->levelUp();
			}

			this->statusBar->updateStatusBar(*this->playerPtr);
		}

		if(!creatureHit || battleDone)
		{
			this->entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY())->movementHandler(event);
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX() - 1, this->playerPtr->getCreaturePositionY(), entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY()));
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY(), nullptr);
			this->playerPtr->setCreaturePosition(sf::Vector2i(this->playerPtr->getCreaturePositionX() - 1, this->playerPtr->getCreaturePositionY()));
		}
	}//Event updae For W press end

	if ((event.key.code == sf::Keyboard::A) && (this->playerPtr->getCreaturePositionY() -1 >= 0))
	{
		while (i < numberOfMonstersInMap && !creatureHit)
		{
			if (monsterArray[i] != nullptr && playerPtr->hit(monsterArray[i], 2))
			{
				creatureHit = true;
			}
			else
			{
				i++;
			}
		}

		if (creatureHit)
		{
			try
			{
				battleDone = playerPtr->battle(*monsterArray[i]);
			}
			catch (char* message)
			{
				cout << "Game Over Triggered" << endl;
				this->gameOver = true;
				this->gameOverScreenCreation();
				this->statusBar->gameOverTrigger();
			}

		}

		if (battleDone)
		{
			int tempI, tempJ;
			tempI = this->monsterArray[i]->getCreaturePositionX();
			tempJ = this->monsterArray[i]->getCreaturePositionY();

			delete this->entityGrid->accessAt(tempI, tempJ);
			this->entityGrid->assignElementAt(tempI, tempJ, nullptr);
			this->monsterArray[i] = nullptr;

			swapElements(monsterArray[i], monsterArray[numberOfMonstersInMap - 1]);
			numberOfMonstersInMap--;

			//Checks if the player has leveld up
			if (this->playerPtr->getXP() >= (playerPtr->getLevel() * 50))
			{
				playerPtr->levelUp();
			}

			this->statusBar->updateStatusBar(*this->playerPtr);
		}

		if(!creatureHit || battleDone)
		{
			this->entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY())->movementHandler(event);
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY()-1, entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY()));
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY(), nullptr);
			this->playerPtr->setCreaturePosition(sf::Vector2i(this->playerPtr->getCreaturePositionX(), playerPtr->getCreaturePositionY()-1));
		}
	}//Event updae For A press end

	if ((event.key.code == sf::Keyboard::S) && (this->playerPtr->getCreaturePositionX()+1 != this->mapHeight))
	{
		while (i < numberOfMonstersInMap && !creatureHit)
		{
			if (monsterArray[i] != nullptr && playerPtr->hit(monsterArray[i], 3))
			{
				creatureHit = true;
			}
			else
			{
				i++;
			}
		}

		if (creatureHit)
		{
			try
			{
				battleDone = playerPtr->battle(*monsterArray[i]);
			}
			catch (char* message)
			{
				cout << "Game Over Triggered" << endl;
				this->gameOver = true;
				this->gameOverScreenCreation();
				this->statusBar->gameOverTrigger();
			}

		}

		if (battleDone)
		{
			int tempI, tempJ;
			tempI = this->monsterArray[i]->getCreaturePositionX();
			tempJ = this->monsterArray[i]->getCreaturePositionY();

			delete this->entityGrid->accessAt(tempI, tempJ);
			this->entityGrid->assignElementAt(tempI, tempJ, nullptr);
			
			swapElements(monsterArray[i], monsterArray[numberOfMonstersInMap - 1]);
			this->monsterArray[numberOfMonstersInMap-1] = nullptr;
			numberOfMonstersInMap--;

			//Checks if the player has leveld up
			if (this->playerPtr->getXP() >= (playerPtr->getLevel() * 50))
			{
				playerPtr->levelUp();
			}

			this->statusBar->updateStatusBar(*this->playerPtr);
		}

		if (!creatureHit || battleDone)
		{
			this->entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY())->movementHandler(event);
			this->entityGrid->assignElementAt(playerPtr->getCreaturePositionX() + 1, this->playerPtr->getCreaturePositionY(), entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY()));
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY(), nullptr);
			this->playerPtr->setCreaturePosition(sf::Vector2i(this->playerPtr->getCreaturePositionX() + 1, this->playerPtr->getCreaturePositionY()));
		}
	}//Event updae For s press end

	if ((event.key.code == sf::Keyboard::D) && (this->playerPtr->getCreaturePositionY()+1 != this->mapWidth))
	{
		while (i < numberOfMonstersInMap && !creatureHit)
		{
			if (monsterArray[i] != nullptr && playerPtr->hit(monsterArray[i], 4))
			{
				creatureHit = true;
			}
			else
			{
				i++;
			}
		}

		if (creatureHit)
		{
			try
			{
				battleDone = playerPtr->battle(*monsterArray[i]);
			}
			catch (char* message)
			{
				cout << "Game Over Triggered" << endl;
				this->gameOver = true;
				this->gameOverScreenCreation();
				this->statusBar->gameOverTrigger();
			}

		}

		if (battleDone)
		{
			int tempI, tempJ;
			tempI = this->monsterArray[i]->getCreaturePositionX();
			tempJ = this->monsterArray[i]->getCreaturePositionY();

			delete this->entityGrid->accessAt(tempI, tempJ);
			this->entityGrid->assignElementAt(tempI, tempJ, nullptr);
			this->monsterArray[i] = nullptr;

			swapElements(monsterArray[i], monsterArray[numberOfMonstersInMap - 1]);
			numberOfMonstersInMap--;

			//Checks if the player has leveld up
			if (this->playerPtr->getXP() >= (playerPtr->getLevel() * 50))
			{
				playerPtr->levelUp();
			}

			this->statusBar->updateStatusBar(*this->playerPtr);

		}

		if (!creatureHit || battleDone)
		{
			this->entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY())->movementHandler(event);
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY()+1, entityGrid->accessAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY()));
			this->entityGrid->assignElementAt(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY(), nullptr);
			this->playerPtr->setCreaturePosition(sf::Vector2i(this->playerPtr->getCreaturePositionX(), this->playerPtr->getCreaturePositionY() + 1));
		}
	}
}//Event updae For D press end

//Function to draw everything in the game
void Game::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	//Draws the map and tileoverlay
	for (int i = 0; i < this->mapHeight; i++)
	{
		for (int j = 0; j < this->mapWidth; j++)
		{
			target.draw(*terrainGrid->accessAt(i, j), states);
			target.draw(tileGridOverlay->accessAt(i, j), states);
		}
	}

	//Draws all the enities onto the map
	for (int i = 0; i < this->mapHeight; i++)
	{
		for (int j = 0; j < this->mapWidth; j++)
		{
			if (this->entityGrid->accessAt(i, j) != nullptr)
			{
				target.draw(*entityGrid->accessAt(i, j), states);
			}
		}
	}

	//Draws the statusbar at the bottom
	target.draw(*statusBar, states);

	//If a gameOverWas triggered a gameover screen will be drawn
	if (this->gameOver)
	{
		target.draw(*this->gameOverScreen, states);
		target.draw(this->gameOvertext, states);
	}
}

//Function to create the gameover screen
void Game::gameOverScreenCreation()
{
	this->gameOverScreen = new sf::RectangleShape(sf::Vector2f((this->mapHeight)*(this->rectangleSideLenght - 20), (this->mapWidth) * (this->rectangleSideLenght - 20)));
	this->gameOverScreen->setFillColor(sf::Color(105, 105, 105));
	this->gameOverScreen->setPosition(this->rectangleSideLenght/2, this->rectangleSideLenght/2);
	this->gameoverFont.loadFromFile("../Resources/times.ttf");
	this->gameOvertext.setFont(gameoverFont);
	this->gameOvertext.setCharacterSize(30);
	this->gameOvertext.setColor(sf::Color::Red);
	this->gameOvertext.setString("GAME OVER");
	this->gameOvertext.setPosition(sf::Vector2f(((this->mapHeight / 2) * this->rectangleSideLenght), ((this->mapWidth / 2) * this->rectangleSideLenght)));
}
