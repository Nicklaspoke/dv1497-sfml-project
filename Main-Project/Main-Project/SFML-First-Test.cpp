#include <iostream>

#include <SFML/Graphics.hpp>

using namespace std;

int main()
{
	sf::RenderWindow window(sf::VideoMode(200, 200), "SFML Works!");	//Creating the window to render on and VideoMode declares size and the second argument is the window title
	sf::CircleShape shape(100.f);	//Creating a circle named shape and declaring it's radius as a float
	shape.setFillColor(sf::Color::Cyan);	//Sets the color of the shape(circle)
	window.setFramerateLimit(120);
	
	sf::Clock mainClock;
	float lastTime = 0;
	while (window.isOpen())	//While loop that runs the program as long as the window is open
	{
		sf::Event event;	//Declaring an system event for the program
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();		//Destroys the window and all it's resources
			}
		}

		float currentTime = mainClock.restart().asSeconds();
		float fps = 1.f / currentTime;
		lastTime = currentTime;
		cout << fps << endl;

		window.clear();
		window.draw(shape);	//Draws the object to the render target, in this case the shape(Circle)
		window.display();	//Displays the rendered material on the screen, is usaly called after a render call

	}

	return 0;
}