#include <iostream>
#include <fstream>
#include <string>

using namespace std;

const int MATRIXSIZE = 4;

int main()
{
	string testmatrix[MATRIXSIZE][MATRIXSIZE];

	ifstream fileRead;
	fileRead.open("Map-Read-Test.txt");

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			getline(fileRead, testmatrix[i][j]);
		}
	}

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << testmatrix[i][j] << "\t";
		}
		cout << endl;
	}

	system("PAUSE");
	return 0;
}