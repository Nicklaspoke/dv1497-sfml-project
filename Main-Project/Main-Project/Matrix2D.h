#pragma once
template<typename T>
class Matrix2D
{
public:
	Matrix2D(int nrOfRows, int nrOfColumns);
	~Matrix2D();

	T accessAt(const int i, const int j);

	void assignElementAt(int row, int column, T element);

	//overloaded operators
	T operator()(const int i, const int j);

private:
	T** matrix;

	int nrOfRows;
	int nrOfColumns;
};

template<typename T>
Matrix2D<T>::Matrix2D(int nrOfRows, int nrOfColumns)
{
	this->nrOfRows = nrOfRows;
	this->nrOfColumns = nrOfColumns;

	this->matrix = new T*[this->nrOfColumns];

	for (int i = 0; i < this->nrOfRows; i++)
	{
		matrix[i] = new T[this->nrOfColumns];
	}
}

template<typename T>
inline Matrix2D<T>::~Matrix2D()
{
	for (int i = 0; i < this->nrOfRows; i++)
	{
		delete[] matrix[i];
	}

	delete[] matrix;
}

template<typename T>
inline T Matrix2D<T>::accessAt(const int i, const int j)
{
	return matrix[i][j];
}

template<typename T>
inline void Matrix2D<T>::assignElementAt(const int row, const int column, T element)
{
	matrix[row][column] = element;
}


//overloaded operators
template<typename T>
inline T Matrix2D<T>::operator()(const int i, const int j)	//overloaded () operator to return the values of the matrix a positionn i and j
{
	return matrix[i][j];
}