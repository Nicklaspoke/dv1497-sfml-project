#include <iostream>

#include "PlayerStatusBar.h"

#include<SFML\Graphics\Color.hpp>

using namespace std;

PlayerStatusBar::PlayerStatusBar(sf::Vector3i position, int offset, Player & infoSource)
{
	//Builds the mainBackround for the statusbar
	this->statusBakround = new sf::RectangleShape(sf::Vector2f(position.z * position.x, position.z));
	this->statusBakround->setFillColor(sf::Color(105, 105, 105));
	this->statusBakround->setPosition(0, (position.x * position.z) + 3);

	//Loads and places the playerIcon sprite
	this->playerIconTexture.loadFromFile("../Resources/Textures/Creature Textures/Littlepip Player Sprite (Right).png");
	this->playerIconSprite.setTexture(this->playerIconTexture);
	this->playerIconSprite.scale(0.8, 0.8);
	this->playerIconSprite.setPosition(3, (position.x*position.z) + 15);

	//Loads font and sets all the base text for the info
	this->infoFont.loadFromFile("../Resources/times.ttf");
	this->nameText.setFont(infoFont);
	this->hpText.setFont(infoFont);
	this->damageText.setFont(infoFont);
	this->moneyText.setFont(infoFont);
	this->xpText.setFont(infoFont);
	this->levelText.setFont(infoFont);
	this->xpNeededForNextLevelText.setFont(infoFont);

	//Sets the fontsize
	this->nameText.setCharacterSize(15);
	this->hpText.setCharacterSize(15);
	this->damageText.setCharacterSize(15);
	this->moneyText.setCharacterSize(15);
	this->xpText.setCharacterSize(15);
	this->levelText.setCharacterSize(15);
	this->xpNeededForNextLevelText.setCharacterSize(15);

	//Sets all the info for the different info texts
	this->nameText.setString("Playername: " + infoSource.getName());
	this->hpText.setString("HP: " + to_string(infoSource.getHP()));
	this->damageText.setString("Damage: " + to_string(infoSource.getPlayerDamage()));
	this->moneyText.setString("Money: " + to_string(infoSource.getMoney()));
	this->xpText.setString("XP: " + to_string(infoSource.getXP()));
	this->levelText.setString("Level: " + to_string(infoSource.getLevel()));
	this->xpNeededForNextLevelText.setString("XP Needed For Next Level: " + to_string(50 * infoSource.getLevel()));

	//Sets all textcolors to black
	this->nameText.setColor(sf::Color::Black);
	this->hpText.setColor(sf::Color::Black);
	this->damageText.setColor(sf::Color::Black);
	this->moneyText.setColor(sf::Color::Black);
	this->xpText.setColor(sf::Color::Black);
	this->levelText.setColor(sf::Color::Black);
	this->xpNeededForNextLevelText.setColor(sf::Color::Black);

	//Sets position for all the stings soo they are in the right place on the statusbar
	this->nameText.setPosition(110, (position.x * position.z) + 5);
	this->hpText.setPosition(110, (position.x * position.z) + 20);
	this->damageText.setPosition(110, (position.x * position.z) + 35);
	this->moneyText.setPosition(110, (position.x * position.z) + 50);
	this->xpText.setPosition(110, (position.x * position.z) + 65);
	this->levelText.setPosition(110, (position.x * position.z) + 80);
	this->xpNeededForNextLevelText.setPosition(110, (position.x * position.z) + 95);
}

PlayerStatusBar::~PlayerStatusBar()
{
	delete this->statusBakround;
}

//Updates the statusbar after each battle
void PlayerStatusBar::updateStatusBar(Player & infoSource)
{
	this->nameText.setString("Playername: " + infoSource.getName());
	this->hpText.setString("HP: " + to_string(infoSource.getHP()));
	this->damageText.setString("Damage: " + to_string(infoSource.getPlayerDamage()));
	this->moneyText.setString("Money: " + to_string(infoSource.getMoney()));
	this->xpText.setString("XP: " + to_string(infoSource.getXP()));
	this->levelText.setString("Level: " + to_string(infoSource.getLevel()));
	this->xpNeededForNextLevelText.setString("XP Needed For Next Level: " + to_string(50 * infoSource.getLevel()));
}

void PlayerStatusBar::gameOverTrigger()
{
	this->playerIconTexture.loadFromFile("../Resources/Textures/Creature Textures/littlepip_gameOver.png");
	this->playerIconSprite.setTexture(playerIconTexture);
}

void PlayerStatusBar::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(*this->statusBakround, states);
	target.draw(this->playerIconSprite, states);
	target.draw(this->nameText, states);
	target.draw(this->hpText, states);
	target.draw(this->damageText, states);
	target.draw(this->moneyText, states);
	target.draw(this->xpText, states);
	target.draw(this->levelText, states);
	target.draw(this->xpNeededForNextLevelText, states);
}
