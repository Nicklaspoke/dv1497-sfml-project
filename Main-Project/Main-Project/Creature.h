#pragma once
#include <string>

#include <SFML\Graphics.hpp>

using namespace std;

class Creature : public sf::Drawable, public sf::Transformable
{
public:
	Creature(sf::Vector3i position, int id = 99, string name = "N/A", int hp = 100, int level = 1);
	~Creature();

	//Accesors and modifiers
	void changeHP(int change);
	int getHP();
	void setHP(int newHP);

	string getName();

	int getLevel();
	void changeLevel(int newLevel);

	void setCreaturePosition(sf::Vector2i position);

	int getCreaturePositionX();
	int getCreaturePositionY();

	bool isAlaive() const;

	//Checks if the the monster to the direction inputed will collide with the player
	bool hit(Creature* monsterTest, int direction);

	//Moves the creature based on the event input
	void movementHandler(sf::Event event);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const;

private:
	string name;

	int hp;
	int level;

	sf::Vector2i creaturePosition;

	sf::Texture creatureTexture;
	sf::Sprite creatureSprite;
};

