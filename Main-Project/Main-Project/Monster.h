#pragma once
#include <string>

#include "Creature.h"

using namespace std;

class Monster : public Creature
{
public:
	Monster(sf::Vector3i position, int id, string name = "N/A", int hp = 10, int level = 1, int damage = 1, int money_drop = 10, int xp_drop = 10);
	~Monster();

	int getDamage();

	int getXPDrop();
	int getMoneyDrop();

private:
	int damage;
	int money_drop;
	int xp_drop;
};

