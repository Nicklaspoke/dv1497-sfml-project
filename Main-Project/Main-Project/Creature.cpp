#include "Creature.h"



Creature::Creature(sf::Vector3i position, int id, string name, int hp, int level)
{
	this->name = name;
	this->hp = hp;
	this->level = level;

	this->creaturePosition.x = position.x;
	this->creaturePosition.y = position.y;

	//Switch to assign to right sprites to the right creature
	switch (id)
	{
	case 0:
	{
		this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Littlepip Player Sprite (Right).png");
		this->creatureSprite.setTexture(this->creatureTexture);
		this->creatureSprite.setPosition(position.z * position.y, position.z * position.x);
		break;
	}//End of case 0

	//Slime
	case 1:
	{
		this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Slime_Sprite_1.png");
		this->creatureSprite.setTexture(creatureTexture);
		this->creatureSprite.setPosition(position.z * position.y, position.z * position.x);
		break;
	}//End of case 1

	//Zombie
	case 2:
	{
		this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Zombie Texture.png");
		this->creatureSprite.setTexture(creatureTexture);
		this->creatureSprite.setPosition(position.z * position.y, position.z * position.x);
		break;
	}//End of case 2

	//Knight
	case 3:
	{
		this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Knight Texture.png");
		this->creatureSprite.setTexture(creatureTexture);
		this->creatureSprite.setPosition(position.z * position.y, position.z * position.x);
		break;
	}//End of case 3

	//Dragon
	case 4:
	{
		this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Dragon Texture.png");
		this->creatureSprite.setTexture(creatureTexture);
		this->creatureSprite.setPosition(position.z * position.y, position.z * position.x);
		break;
	}//End of case 4
	}//End of switch
}

Creature::~Creature()
{
}

//Changes the HP of the monster/player with the amount set is
void Creature::changeHP(int change)
{
	this->hp += change;
}

int Creature::getHP()
{
	return this->hp;
}

void Creature::setHP(int newHP)
{
	this->hp = newHP;
}

string Creature::getName()
{
	return this->name;
}

int Creature::getLevel()
{
	return this->level;
}

void Creature::changeLevel(int newLevel)
{
	this->level = newLevel;
}

void Creature::setCreaturePosition(sf::Vector2i position)
{
	this->creaturePosition.x = position.x;
	this->creaturePosition.y = position.y;
}

int Creature::getCreaturePositionX()
{
	return this->creaturePosition.x;
}

int Creature::getCreaturePositionY()
{
	return this->creaturePosition.y;
}

//Returns of creatures hp is > 0 aka creature is alive
bool Creature::isAlaive() const
{
	return this->hp > 0;
}

bool Creature::hit(Creature * monsterTest, int direction)
{
	bool enemyHit = false;

	switch (direction)
	{
	//Case 1 Up, aka W pressed
	case 1:
	{
		if (this->creaturePosition.x - 1 == monsterTest->creaturePosition.x && this->creaturePosition.y == monsterTest->creaturePosition.y)
		{
			enemyHit = true;
		}
		break;
	}//End of case 1

	//Case  2 Left, aka A pressed
	case 2:
	{
		if (this->creaturePosition.y - 1 == monsterTest->creaturePosition.y && this->creaturePosition.x == monsterTest->creaturePosition.x)
		{
			enemyHit = true;
		}
		break;
	}//End of case 2

	//Case 3 Down, aka S pressed
	case 3:
	{
		if (this->creaturePosition.x + 1 == monsterTest->creaturePosition.x && this->creaturePosition.y == monsterTest->creaturePosition.y)
		{
			enemyHit = true;
		}
		break;
	}

	//Case 4 Right, aka D pressed
	case 4:
	{
		if (this->creaturePosition.y + 1 == monsterTest->creaturePosition.y && this->creaturePosition.x == monsterTest->creaturePosition.x)
		{
			enemyHit = true;
		}
		break;
	}
	}//End of switch

	return enemyHit;
}

void Creature::movementHandler(sf::Event event)
{
	switch (event.type)
	{
	case sf::Event::KeyPressed:
	{
		if (event.key.code == sf::Keyboard::W)
		{
			this->creatureSprite.move(0, -120);
		}

		if (event.key.code == sf::Keyboard::S)
		{
			this->creatureSprite.move(0, 120);
		}

		if (event.key.code == sf::Keyboard::A)
		{
			this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Littlepip Player Sprite (Left).png");
			this->creatureTexture.setSmooth(true);
			this->creatureSprite.setTexture(creatureTexture);
			this->creatureSprite.move(-120, 0);
		}

		if (event.key.code == sf::Keyboard::D)
		{
			this->creatureTexture.loadFromFile("../Resources/Textures/Creature Textures/Littlepip Player Sprite (Right).png");
			this->creatureSprite.setTexture(creatureTexture);
			this->creatureSprite.move(120, 0);
		}
		break;
	}
	}

}

void Creature::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(this->creatureSprite, states);
}


