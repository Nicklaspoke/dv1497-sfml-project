#pragma once

#include "Player.h"

#include <SFML\Graphics.hpp>

using namespace std;

class PlayerStatusBar : public sf::Drawable, public sf::Transformable
{
public:
	PlayerStatusBar(sf::Vector3i position, int offset, Player &infoSource);
	~PlayerStatusBar();

	void updateStatusBar(Player &infosource);

	void gameOverTrigger();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const;

private:
	sf::RectangleShape* statusBakround;
	sf::Font infoFont;
	sf::Text nameText;
	sf::Text hpText;
	sf::Text damageText;
	sf::Text moneyText;
	sf::Text xpText;
	sf::Text levelText;
	sf::Text xpNeededForNextLevelText;

	sf::Texture playerIconTexture;
	sf::Sprite playerIconSprite;

};

