#include <iostream>
#include <vector>

#include "Matrix2D.h"

using namespace std;

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	/*vector< vector<int> > testMatrix;

	int m = 2;
	int n = 4;

	testMatrix.resize(m);

	for (int i = 0; i < m; i++)
	{
		testMatrix[i].resize(n);
	}

	testMatrix[0][1] = 2;

	cout << testMatrix[0][1] << endl;*/

	Matrix2D<int> testMatrix(5,6);

	testMatrix.assignElementAt(0, 0, 1);

	cout << testMatrix(0, 0) << endl;

	system("pause");

	return 0;
}