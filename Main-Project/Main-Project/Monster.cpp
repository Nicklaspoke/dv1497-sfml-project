#include "Monster.h"

Monster::Monster(sf::Vector3i position, int id, string name, int hp, int level, int damage, int money_drop, int xp_drop) : Creature(position,id, name, hp, level)
{
	this->damage = damage;
	this->money_drop = money_drop;
	this->xp_drop = xp_drop;
}

Monster::~Monster()
{
}

int Monster::getDamage()
{
	return this->damage;
}

int Monster::getXPDrop()
{
	return this->xp_drop;
}

int Monster::getMoneyDrop()
{
	return this->money_drop;
}
