#include <iostream>
#include <fstream>

#include <SFML\Graphics.hpp>

#include "Game.h"

using namespace std;

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	Game mainGame;

	int windowWidth = mainGame.getMapSize().x * mainGame.getRectangleSideLength();
	int windowHeight = mainGame.getMapSize().y * mainGame.getRectangleSideLength() + mainGame.getOffSet();

	sf::RenderWindow mainWindow(sf::VideoMode(windowWidth, windowHeight), mainGame.getMapName());

	sf::Event event;

	while (mainWindow.isOpen())
	{
		

		while (mainWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				mainWindow.close();		//Destroys the window and all it's resources
			}

			if (event.type == sf::Event::KeyPressed)
			{
				mainGame.update(event);
			}
		}


		
		mainWindow.clear();
		mainWindow.draw(mainGame);
		mainWindow.display();
	}
}