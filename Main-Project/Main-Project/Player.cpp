#include <iostream>

#include <SFML\Graphics.hpp>

#include "Player.h"

using namespace std;

//Constructor
Player::Player(sf::Vector3i position, int id, string name, int hp, int level, int money, int xp, int damage) : Creature(position, id, name, hp, level)
{
	this->money = money;
	this->xp = xp;
	this->playerDamage = damage;
}

//Destructor
Player::~Player()
{
}

int Player::getMoney()
{
	return this->money;
}

int Player::getXP()
{
	return this->xp;
}

int Player::getPlayerDamage()
{
	return this->playerDamage;
}

void Player::changeXP(int xp_change)
{
	this->xp += xp_change;
}

void Player::changeMoney(int money_change)
{
	this->money += money_change;
}

void Player::levelUp()
{
	int newHP = 100;	//The hp will increase with 20% for each level the player has 
	
	for (int i = 0; i < this->getLevel() + 1; i++)
	{
		newHP *= 1.20;
	}

	if ((this->getLevel() * 50) < this->xp)
	{
		this->xp = (this->xp - (this->getLevel() * 50));
	}
	else
	{
		this->xp = 0;
	}

	this->changeLevel(this->getLevel() + 1);
	this->setHP(newHP);
	this->playerDamage = playerDamage + 10;

}

bool Player::battle(Monster& monsterToBattle)
{
	bool battleSuccess = false;
	while (this->isAlaive() && monsterToBattle.isAlaive())
	{
		this->changeHP(-monsterToBattle.getDamage());
		monsterToBattle.changeHP(-this->playerDamage);
	}

	if (!this->isAlaive())
	{
		throw "Game Over";
	}
	else
	{
		battleSuccess = true;
		this->changeXP(monsterToBattle.getXPDrop());
		this->changeMoney(monsterToBattle.getMoneyDrop());
	}
}
