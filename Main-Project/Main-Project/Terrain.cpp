//Disclaimer, all textures used in this project are free to use textures from
//freestocktextures.com

#include "Terrain.h"

Terrain::Terrain(bool isPassable)
{
	this->isPassable = isPassable;
}

Terrain::Terrain(string texture, const int i, const int j, const int rectangeLength)
{
	if (texture == "Grass")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Grass Texture.jpg");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j);
	}
	else if (texture == "Stone")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Stone Texture.jpg");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j);
	}
	else if (texture == "Brown Ground 1")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Brown Ground Texture 1.jpg");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j);
	}
	else if (texture == "Clay")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Clay Texture 1.jpg");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setRotation(270); //Adjustment cause the loaded cause the loaded texture was at the wrong rotation
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j + rectangeLength);
	}
	else if (texture == "Grass With Weeds")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Grass With Weeds Texture.jpg");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j);
	}
	else if (texture == "Moss")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Moss Texture.jpg");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j);
	}
	else if (texture == "Marble")
	{
		this->terrainTexture.loadFromFile("../Resources/Textures/Terrain Textures/Marble Terrain Texture.png");
		this->terrain.setTexture(terrainTexture);
		this->terrain.setPosition(rectangeLength *i, rectangeLength * j);
	}
}

void Terrain::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(this->terrain, states);
}

Terrain::~Terrain()
{
}