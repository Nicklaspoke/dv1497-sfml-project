#pragma once
//This file contains useful help functions like randomzing int values and other useful stuff the whole program can use

int randomize(int min_value, int max_value);

template<typename T>
void swapElements(T &a, T &b)
{
	T swaptemp;

	swaptemp = a;
	a = b;
	b = swaptemp;
}
