#include <iostream>

#include <SFML\Graphics.hpp>

#include "Player.h"

using namespace std;

const float RECTANGLEWITH = 120;
const float RECTANGLEHEIGHT = 120;

const int WINDOWHEIGHT = RECTANGLEHEIGHT * 5;
const int WINDOWWITH = RECTANGLEWITH * 5;

int main()
{
	sf::RectangleShape mainMap[5][5];

	sf::RenderWindow mainWindow(sf::VideoMode(WINDOWWITH, WINDOWHEIGHT), "Player Sprite Move Test");

	mainWindow.setFramerateLimit(120);

	Player testplayer("Test Player", 100, 1, 1000, 0);
	
	//Loop for creating the map tiles
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			sf::RectangleShape iRecShape;
			iRecShape.setSize(sf::Vector2f(RECTANGLEHEIGHT, RECTANGLEWITH));
			//iRecShape.setFillColor(sf::Color::Green);
			iRecShape.setOutlineThickness(1);
			iRecShape.setOutlineColor(sf::Color::Black);
			iRecShape.setPosition(RECTANGLEHEIGHT * i, RECTANGLEWITH * j);
			mainMap[i][j] = iRecShape;
		}
	}

	while (mainWindow.isOpen())
	{
		sf::Event event;

		while (mainWindow.pollEvent(event))
		{
			//Switch to handle all the events
			switch (event.type)
			{
				//Case for destroying the mainWindow
			case sf::Event::Closed:
			{
				mainWindow.close();
				break;
			}

			default:
			{
				break;
			}
			}//End of switch statement

			if (sf::Event::KeyPressed)
			{
				testplayer.movementHandler(event);	//Calling the movement handler for the player object when a movement event is called
			}
		
		}//End of loop for pollEvent

		 //mainWindow.clear();
		mainWindow.clear();

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				mainWindow.draw(mainMap[i][j]);
			}
		}
		//mainWindow.draw(player);
		mainWindow.draw(testplayer);
		mainWindow.display();
	}
}